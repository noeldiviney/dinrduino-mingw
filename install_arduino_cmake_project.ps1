#!/usr/bin/env pwsh
###  #!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

#param([string]$OS           = "WIN",
#      [string]$DRIVE        = "DRIVE",
#      [string]$SLASH        = "SLASH");

#$SLASH                      = ""
#$BASE_ROOT                  = ""


#	if(${OS} -eq "WIN")
#    { 
#        / = "\";
#	    ${BASE_ROOT} = "${DRIVE}:/"	
#    }

$OS_UC                         = "LIN"
$OS_LC                         = "lin"
$OS_C                          = "Lin"
$DRIVE                         = "/home/eicon"
$SLASH                         = "/"
$INSTALL_FOLDER                = "DinRDuino"
$PROJECT_FOLDER                = "Projects"
$PROJECT_NAME                  = "F103CBT6-HelloW"
$PROJECT_RELEASE               = "2.2.0"
$STM32_CORE_NAME               = "Core_STM32"
$ARDUINO_VERSION               = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE     = "1.0.1"
$ARDUINO_CLI_RELEASE           = "0.20.2"
$VENDOR_EI                     = "Eicon"
$VENDOR_EI_LC                  = "eicon"
$BOARD_VENDOR_ST               = "STMicroelectronics"
$SKETCHBOOK_RELEASE            = "0.1.0"
$OPENOCD_RELEASE               = "0.11.0-1"
$OPENOCD_CFG_RELEASE           = "0.1.0"
$ARM_GCC_VERSION               = "10.1.0"
$TEENSY_GCC_VERSION            = "5.4.1"
$CMSIS_VERSION                 = "5.7.0"

$PROJECT_URI                   = "https://gitlab.com/noeldiviney/${PROJECT_NAME}-${OS_C}/-/archive/${PROJECT_RELEASE}/${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}.zip"
$VARIANTS_URI                  = "https://gitlab.com/noeldiviney/${VENDOR_EI_LC}_${OS_LC}_stm32_variants/-/archive/${PROJECT_RELEASE}/${VENDOR_EI_LC}_${OS_LC}_stm32_variants-${PROJECT_RELEASE}.zip"
$STM32_CORE_ZIP_URI            = "https://github.com/stm32duino/Arduino_${STM32_CORE_NAME}/archive/refs/tags/${PROJECT_RELEASE}.zip"
## https://github.com/stm32duino/Arduino_Core_STM32/archive/refs/tags/2.2.0.zip
$PROJECT_ZIP_NAME              = "${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}.zip"
$PROJECT_UNZIPPED_NAME         = "${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}"
$ARDUINO_STM32_CORE_ZIP_NAME   = "Arduino_${STM32_CORE_NAME}-${PROJECT_RELEASE}.zip"
$ARDUINO_STM32_CORE_NAME       = "Arduino_${STM32_CORE_NAME}-${PROJECT_RELEASE}"
$VARIANTS_ZIP_NAME             = "${VENDOR_EI_LC}_${OS_LC}_stm32_variants-${PROJECT_RELEASE}.zip"
$VARIANTS_NAME                 = "${VENDOR_EI_LC}_${OS_LC}_stm32_variants-${PROJECT_RELEASE}"


$OPENOCD_TB_TYPE               = "win32-x64.zip"
$ARDUINO_LIBRARIES_PREFIX      = "${VENDOR_EI_LC}_win_arduino_libraries"
$SKETCHBOOK_NAME_PREFIX        = "${VENDOR_EI_LC}_${OS_LC}_sketchbook"
$XPACK_GCC_NAME                = "xpack-arm-none-eabi-gcc"
##${VENDOR_EI_LC}_GCC_NAME      = "${VENDOR_EI_LC}-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_TYPE           = "Windows_64bit.zip"
$ARDUINO_CLI_TB_NAME           = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$ARDUINO_TB_TYPE               = "windows.zip"
$OPENOCD_CFG_SCRIPT_NAME       = "${VENDOR_EI_LC}_${OS_LC}_openocd_cfg"

$BASE_PATH                     = "${DRIVE}/"
$DINRDUINO_PATH                = "${BASE_PATH}/${INSTALL_FOLDER}"
$DLOAD_PATH                    = "${BASE_PATH}${INSTALL_FOLDER}/dload" 
$PROJECT_PATH                  = "${BASE_PATH}${INSTALL_FOLDER}/Projects"
$ARDUINO_PATH                  = "${BASE_PATH}/arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH                 = "${ARDUINO_PATH}/portable"
$HARDWARE_PATH_EI              = "${PORTABLE_PATH}/packages/${VENDOR_EI}/hardware"
$HARDWARE_PATH_ST              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/hardware"
$TOOLS_PATH_EI                 = "${PORTABLE_PATH}/packages/${VENDOR_EI}/tools"
$TOOLS_PATH_ST                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/tools"
$OPENOCD_PATH                  = "${BASE_PATH}/Openocd"
#$DLOAD_PATH                    = "${BASE_PATH}/dload"
$PREFS_PATH                    = "${PORTABLE_PATH}"
$SKETCH_PATH                   = "${PORTABLE_PATH}/sketchbook/arduino"
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
$ARDUINO_CLI_URI               = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI        = "https://gitlab.com/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI     = "https://gitlab.com/noeldiviney/${VENDOR_EI_LC}_${OS_LC}_arduino_libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$ARDUINO_ZIP_URI               = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$SKETCHBOOK_ZIP_URI            = "https://gitlab.com/noeldiviney/${VENDOR_EI_LC}_${OS_LC}_sketchbook/-/archive/${SKETCHBOOK_RELEASE}/${VENDOR_EI_LC}_${OS_LC}_sketchbook-${SKETCHBOOK_RELEASE}.zip"
$OPENOCD_TB_NAME               = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI                = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$STM32_INDEX                   = "        https://gitlab.com/noeldiviney/${VENDOR_EI_LC}_${OS_LC}_packages/-/raw/main/package_${VENDOR_EI_LC}_stm32_index.json,"
$KINETIS_INDEX                 = "        https://gitlab.com/noeldiviney/${VENDOR_EI_LC}_${OS_LC}_packages/-/raw/main/package_${VENDOR_EI_LC}_kinetis_index.json,"
$STMicroelectronics_INDEX      = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE              = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE    = "${VENDOR_EI_LC}_${OS_LC}_arduino_libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                   = "${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                          = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH                  = "/home/$env:USER"
$SCRIPT_PATH                   = "${BASE_ROOT}bin"
$OPENOCD_ZIP_FILE              = "${VENDOR_EI_LC}-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE          = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE           = "sketchbook.zip"
$SourceFileLocation            = "${BASE_ROOT}Program Files/Notepad++/notepad++.exe"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     main"; 

    Write-Host "Line $(CurrentLine)  Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      check_environment";
    check_environment
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cmake_project";
    install_arduino_cmake_project
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_stm32_core";
    install_arduino_stm32_core
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      install_eicon_win_stm32_variants";
    install_eicon_win_stm32_variants
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# install_DesktopShortcut
#---------------------------------------------------------
function install_DesktopShortcut
{
#$WScriptShell = New-Object -ComObject WScript.Shell
    Write-Host "Line $(CurrentLine)  Entering                     install_DesktopShortcut";
#    $SourceFileLocation = "C:/Program Files/PowerShell/7/pwsh.exe"
#    $ShortcutLocation = "C:/Users/$USER/Desktop/pwsh.exe.lnk"
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    $Shortcut = "$WScriptShell.CreateShortcut($ShortcutLocation)" 
#    $Shortcut.TargetPath = $SourceFileLocation "C:/Program Files/PwrShell/arduino-install.ps1" "W Dinrduino5 1.8.15 Eicon"
#    $Shortcut.Save()
#    Write-Host "Line $(CurrentLine)  Executing                 $ShortcutLocation = "C:/Users/$USER/Desktop/Notepad++.lnk";
#    $ShortcutLocation = "C:/Users/$USER/Desktop/Notepad++.lnk"
#    Write-Host "Line $(CurrentLine)  Executing                 $WScriptShell = New-Object -ComObject WScript.Shell";                 
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    Write-Host "Line $(CurrentLine)  Executing                 '$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)'";
#    $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.TargetPath = $SourceFileLocation";
#    $Shortcut.TargetPath = $SourceFileLocation
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.Save()";
#    $Shortcut.Save()
    Write-Host "Line $(CurrentLine)  Leaving                      install_DesktopShortcut";
}


#---------------------------------------------------------
# install_eicon_win_stm32_variants
#---------------------------------------------------------
function install_eicon_win_stm32_variants
{
    Write-Host "Line $(CurrentLine)  Entering                     install_eicon_win_stm32_variants";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/variants)
    {
        Write-Host "Line $(CurrentLine)  variants                     exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/variants" ;  
        Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                     do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/boards.txt" ;  
        Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   does not exist  ...  continuing" ;  
    }

    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/platform.txt" ;  
        Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}/platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip             exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_ZIP+NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}        
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}/${VARIANTS_NAME})";
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants unzipped            exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_NAME}        
    }
		
    Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
	
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}/${VARIANTS_NAME}/*" -destination: ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}";
    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}/${VARIANTS_NAME}/*" -destination: ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}
	
    Write-Host "Line $(CurrentLine)  Leaving                      install_eicon_lin_stm32_variants";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}




#---------------------------------------------------------
# install_arduino_stm32_core
#---------------------------------------------------------
function install_arduino_stm32_core
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_stm32_core";


    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${STM32_CORE_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${STM32_CORE_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core                       is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME} -DestinationPath ${PROJECT_PATH}/${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_STM32_CORE_ZIP_NAME} -DestinationPath ${PROJECT_PATH}/${PROJECT_NAME}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                     ${PROJECT_PATH}/${PROJECT_NAME}/${ARDUINO_STM32_CORE_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${PROJECT_PATH}/${PROJECT_NAME}/${ARDUINO_STM32_CORE_NAME}  ${PROJECT_PATH}/${PROJECT_NAME}/${STM32_CORE_NAME}
		
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_stm32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_arduino_cmake_project
#---------------------------------------------------------
function install_arduino_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${PROJECT_PATH}/${PROJECT_NAME})
    {
        Write-Host "Line $(CurrentLine)  Project                      Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${PROJECT_PATH}/${PROJECT_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                      does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                   ${PROJECT_PATH}/${PROJECT_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${PROJECT_URI} -Outfile ${DLOAD_PATH}/${PROJECT_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${PROJECT_URI} -Outfile ${DLOAD_PATH}/${PROJECT_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}/${PROJECT_ZIP_NAME} -DestinationPath ${PROJECT_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${PROJECT_ZIP_NAME} -DestinationPath ${PROJECT_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                     ${PROJECT_PATH}/${PROJECT_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${PROJECT_PATH}/${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${PROJECT_PATH}/${PROJECT_UNZIPPED_NAME}  ${PROJECT_PATH}/${PROJECT_NAME}

    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# check_environment
#---------------------------------------------------------
function check_environment
{
    Write-Host "Line $(CurrentLine)  Entering                     check_environment";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DINRDUINO_PATH})";
    if (Test-Path ${DINRDUINO_PATH})
    {
        Write-Host "Line $(CurrentLine)  DinRDuino Path               Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${DINRDUINO_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${DINRDUINO_PATH}";
        md ${DINRDUINO_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH})";
    if (Test-Path ${DLOAD_PATH})
    {
        Write-Host "Line $(CurrentLine)  dload folser                 Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md $DLOAD_PATH}";
        md ${DLOAD_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH})";
    if (Test-Path ${PROJECT_PATH})
    {
        Write-Host "Line $(CurrentLine)  Projects Folder              Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${PROJECT_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${PROJECT_PATH}";
        md ${PROJECT_PATH}
    }

    Write-Host "Line $(CurrentLine)  Leaving                      check_environment";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                        echo_args";
    Write-Host "Line $(CurrentLine)  Operating System              = ${OS}";
    Write-Host "Line $(CurrentLine)  DRIVE                         = ${DRIVE}";
    Write-Host "Line $(CurrentLine)  SLASH                         = /";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER                = ${INSTALL_FOLDER}";	
    Write-Host "Line $(CurrentLine)  PROJECT_FOLDER                = ${PROJECT_FOLDER}";	
    Write-Host "Line $(CurrentLine)  PROJECT_NAME                  = ${PROJECT_NAME}";	
    Write-Host "Line $(CurrentLine)  PROJECT_RELEASE               = ${PROJECT_RELEASE}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE     = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  VENDOR_EI                     = ${VENDOR_EI}";            
    Write-Host "Line $(CurrentLine)  VENDOR_ST                     = ${VENDOR_ST}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_RELEASE            = ${SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE               = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_RELEASE           = ${OPENOCD_CFG_RELEASE}";            
    Write-Host "Line $(CurrentLine)  ARM_GCC_VERSION               = ${ARM_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  TEENSY_GCC_VERSION            = ${TEENSY_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  CMSIS_VERSION                 = ${CMSIS_VERSION}";            
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                    = ${DLOAD_PATH}";	
    Write-Host "Line $(CurrentLine)  PROJECT_PATH                  = ${PROJECT_PATH}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH                  = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH                 = ${PORTABLE_PATH}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_EI              = ${HARDWARE_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_ST              = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_EI                 = ${TOOLS_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_ST                 = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                    = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                   = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";

	Write-Host "Line $(CurrentLine)  PROJECT_URI                   = ${PROJECT_URI}";
	Write-Host "Line $(CurrentLine)  VARIANTS_URI                  = ${VARIANTS_URI}";
    Write-Host "Line $(CurrentLine)  STM32_CORE_ZIP_URI            = ${STM32_CORE_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  PROJECT_ZIP_NAME              = ${PROJECT_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_UNZIPPED_NAME         = ${PROJECT_UNZIPPED_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_STM32_CORE_ZIP_NAME   = ${ARDUINO_STM32_CORE_ZIP_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_STM32_CORE_NAME       = ${ARDUINO_STM32_CORE_NAME}";            
	Write-Host "Line $(CurrentLine)  VARIANTS_ZIP_NAME             = ${VARIANTS_ZIP_NAME}";
	Write-Host "Line $(CurrentLine)  VARIANTS_NAME                 = ${VARIANTS_NAME}";
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_TYPE               = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_NAME                = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_TYPE           = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_NAME           = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_TB_TYPE               = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NANE               = ${OPENOCD_TB_NAME}";                    
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE           = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                         echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Setup OS Stuff
#---------------------------------------------------------
function setup_os_stuff
{
    Write-Host "Line $(CurrentLine)  Entering                     setup_os_stuff"
	if(${OS} -eq "WIN")
    { 
        Write-Host "Line $(CurrentLine)  OS                         = Windows";            
        Write-Host "Line $(CurrentLine)  SLASH                      = /";
        Write-Host "Line $(CurrentLine)  ARDUINO_VERSION            = ${ARDUINO_VERSION}";
        Write-Host "Line $(CurrentLine)  SLASH                      = /";
        / = "\";
        Write-Host "Line $(CurrentLine)  SLASH                      = /";
        ${BASE_ROOT} = "${DRIVE}:/"	
        Write-Host "Line $(CurrentLine)  BASE_ROOT                  = ${BASE_ROOT}";
    }
    elseIf(${OS} -eq "LIN")
    {
        Write-Host "Line $(CurrentLine)  OS                         = Linux";            
    }
    ElseIf(${OS} -eq "WSL" )
    {
        Write-Host "Line $(CurrentLine)  OS                         = WSL";            
    }
    Else
    {
        Write-Host "Line $(CurrentLine)  OS                         = Not Defined";            
    }
    Write-Host "Line $(CurrentLine)  Leaving                      setup_os_stuff"
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                  Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                 $ARDUINO_PATH/arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH/arduino
    Write-Host "Line $(CurrentLine)  Leaving                   Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"


